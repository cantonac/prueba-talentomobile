var express = require('express');
var router = express.Router();
const userController = require('../controllers/users');

/* Users API. */
router.get('/', userController.list);
router.post('/', userController.add);
router.put('/:id', userController.update);
router.delete('/:id', userController.delete);

module.exports = router;
