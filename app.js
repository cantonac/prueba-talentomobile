var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var jwt = require('jsonwebtoken');

var loginRouter = require('./routes/login');
var usersRouter = require('./routes/users');

var app = express();

// DB connection
const mongoose = require("mongoose");
mongoose.Promise = global.Promise;
mongoose.set('useFindAndModify', false);
mongoose.connect('mongodb+srv://cantona:talentomobile@clustermongodb-yx1ez.mongodb.net/talentomobile?retryWrites=true&w=majority', { useNewUrlParser: true });
mongoose.connection.on('error', console.error.bind(console, 'MongoDB connection error:'));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// JWT secret token
app.set('secretKey', 'talentomobileApi');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Print endpoint and date manually
/*app.use(function (req, res, next) {
  console.log("[" + new Date() + "] - " + req.method + " " + req.originalUrl);
  next();
});*/

// Print endpoint and date using Morgan
app.use(logger('[:date[clf]] :method :url :status'))

app.use('/login', loginRouter);
app.use('/users', validateUser, usersRouter);

// Validate JWT
function validateUser(req, res, next) {
  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function (err, decoded) {
    if (err) {
      res.json({ status: "ERR", data: null });
    } else {
      req.body.jwtid = decoded.id;
      next();
    }
  });
}

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
