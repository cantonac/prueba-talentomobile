let chai = require('chai');
let chaiHttp = require('chai-http');
const expect = require('chai').expect;

chai.use(chaiHttp);
const url= 'http://localhost:3000';

var jwt = "";
var idTest = "";

describe('-> LOGIN USER: ',()=>{

	it('Should login a user and return a jwt', (done) => {
		chai.request(url)
			.post('/login')
			.send({username: "Carlos", password: "1234"})
			.end( function(err,res){
				console.log(res.body)
				expect(res).to.have.status(200);
				expect(res.body.status).to.be.equal("OK");
                jwt = res.body.data.token;
				done();
			});
	});
});

describe('-> INSERT A USER: ',()=>{

	it('Should insert a user', (done) => {
		chai.request(url)
			.post('/users')
			.set('x-access-token', jwt)
			.send({username: "test", password: "password"})
			.end( function(err,res){
				console.log(res.body)
				expect(res).to.have.status(200);
				expect(res.body.status).to.be.equal("OK");
                idTest = res.body.data._id;
				done();
			});
	});
});

describe('-> GET ALL USERS: ',()=>{
	it('Should get all users', (done) => {
		chai.request(url)
			.get('/users')
			.set('x-access-token', jwt)
			.end( function(err,res){
				console.log(res.body)
				expect(res).to.have.status(200);
				expect(res.body.status).to.be.equal("OK");
				done();
			});
	});

});

describe('-> UPDATE THE INSERTED USER : ',()=>{

	it('Should update the user test', (done) => {
		chai.request(url)
			.put('/users/' + idTest)
			.set('x-access-token', jwt)
            .send({username: "test_updated", password: "password_updated"})
			.end( function(err,res){
                console.log(res.body)
				expect(res).to.have.status(200);
				expect(res.body.status).to.be.equal("OK");
				expect(res.body.data).to.have.property('username').to.be.equal("test_updated");
				done();
			});
	});

});

describe('-> DELETE THE INSERTED/UPDATED USER: ',()=>{

	it('Should delete the user with id ' + idTest, (done) => {
		chai.request(url)
			.get('/users')
			.set('x-access-token', jwt)
			.end( function(err,res){
				console.log(res.body)
				expect(res.body.data).to.have.lengthOf(3);
				expect(res).to.have.status(200);
				chai.request(url)
					.del('/users/' + idTest)
					.set('x-access-token', jwt)
					.end( function(err,res){
						console.log(res.body)
						expect(res).to.have.status(200);
						expect(res.body.status).to.be.equal("OK");
						chai.request(url)
							.get('/users')
							.set('x-access-token', jwt)
							.end( function(err,res){
								console.log(res.body)
								expect(res.body.data).to.have.lengthOf(2);
								expect(res).to.have.status(200);
								done();
						});
					});
			});
	});

});
