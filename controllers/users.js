const userModel = require('../models/users');

module.exports = {
    list: function (req, res, next) {
        let userList = [];
        userModel.find({}, function (err, users) {
            if (err) {
                next(err);
            } else {
                for (let user of users) {
                    userList.push({_id: user._id, username: user.username, password: user.password});
                }
                res.json({ status: "OK", data: userList });
            }
        });
    },
    add: function (req, res, next) {
        userModel.create({ username: req.body.username, password: req.body.password}, function (err, result) {
            if (err)
                next(err);
            else
                res.json({ status: "OK", data: { _id: result._id, username: req.body.username, password: req.body.password } });
        });
    },
    update: function (req, res, next) {
        userModel.findByIdAndUpdate(req.params.id, { username: req.body.username, password: req.body.password }, function (err, result) {
            if (err)
                next(err);
            else {
                res.json({ status: "OK", data: { _id: result._id, username: req.body.username, password: req.body.password } });
            }
        });
    },
    delete: function (req, res, next) {
        userModel.findByIdAndRemove(req.params.id, function (err, result) {
            if (err)
                next(err);
            else {
                res.json({ status: "OK", data: {_id: req.params.id} });
            }
        });
    }
}