const userModel = require('../models/users');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

module.exports = {
    login: function (req, res, next) {
        userModel.findOne({ username: req.body.username }, function (err, userInfo) {
            if (err) {
                next(err);
            } else {
                if (userInfo != null && req.body.password && bcrypt.compareSync(req.body.password, userInfo.password)) {
                    const token = jwt.sign({ id: userInfo._id }, req.app.get('secretKey'), { expiresIn: '1h' });
                    res.json({ status: "OK", data: { user: userInfo, token: token } });
                } else {
                    res.json({ status: "ERR", data: null });
                }
            }
        });
    }
}