const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const salt = 10;

// Define user schema
const Schema = mongoose.Schema;
const UserSchema = new Schema({
 username: {
  type: String,
  trim: true,  
  required: true,
 },
 password: {
  type: String,
  trim: true,
  required: true
 }
});


// Hash password before save it
UserSchema.pre('save', function(next){
    this.password = bcrypt.hashSync(this.password, salt);
    next();
});

UserSchema.pre('findOneAndUpdate', function(next){
    this._update.password = bcrypt.hashSync(this._update.password, salt);
    next();
});
    
module.exports = mongoose.model('User', UserSchema);