## Prueba Técnica NODE.JS
1. [[314795a](https://bitbucket.org/cantonac/prueba-talentomobile/commits/314795ad35837f4c7824b744ecf08d011de748e6)] Crear un proyecto base en Node - Express con un servicio GET llamado /users que devuelva un array de usuarios con la siguiente estructura:
```
{
    [
        {
        “username”: “valor”,
        “password”: “valor”
        }
    ]
}
```
2. [[6a0cf52](https://bitbucket.org/cantonac/prueba-talentomobile/commits/6a0cf523d12ed1cd5505cbde04e9d7b45c733ade)] Crear un API REST para añadir, actualizar y eliminar nuevos usuarios.
3. [[c08058a](https://bitbucket.org/cantonac/prueba-talentomobile/commits/c08058a39b5d216961812e9a9b36f891b642d977)] Crear un servicio para autenticar al usuario (mediante usuario y password). Devolverá un JWT.
4. [[67e36eb](https://bitbucket.org/cantonac/prueba-talentomobile/commits/67e36eb4b76ae5458a9b0770e52b972aac0169e6)] Crear un middleware para autenticar las peticiones a las API REST (Excepto Login), haciendo uso del JWT.
5. [[53f463c](https://bitbucket.org/cantonac/prueba-talentomobile/commits/53f463c22b8eb6ee33e84c556a74f97baaa4fbfb)] Crear un middleware que muestre por consola la siguiente información de cada petición:
    - Endpoint.
    - fecha y hora.

Deberá subirse a bitbucket y realizar diversos commit por cada requerimiento.

[[8be7429](https://bitbucket.org/cantonac/prueba-talentomobile/commits/8be7429843574c88db9a7349973b188908f9dc2e)] *Opcional (a valorar): Implementar test*
